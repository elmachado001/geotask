package com.example.elmac.listageoreferenciada

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        android.R.drawable.toast_frame

    }


    fun irTareas(view: View){
        startActivity(Intent(this, TareasActivity::class.java))
    }

    fun irListas(view: View){
        startActivity(Intent(this, ListasActivity::class.java))
    }
    fun irGrupos(view: View){
        startActivity(Intent(this, GruposActivity::class.java))
    }

    fun irMapas(view: View){
        startActivity(Intent(this, MapsActivity::class.java))
    }
}
