package com.example.elmac.listageoreferenciada

import android.app.Activity
import android.content.Intent
import android.graphics.Canvas
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class LoginActivity : AppCompatActivity() {

    private lateinit var txtUser: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        txtUser = findViewById(R.id.txtUser)
        txtPassword = findViewById(R.id.txtPassword)
        progressBar = findViewById(R.id.progressBar)
        auth=FirebaseAuth.getInstance()

    }

    fun forgotPassword (view: View){
            startActivity(Intent(this,ForgotPasswordActivity::class.java))

    }
    fun registrer (view: View){
            startActivity(Intent(this, RegisterActivity::class.java))

    }
    fun login (view: View){

        loginUser()
    }

    fun loginUser (){
        val user:String = txtUser.text.toString()
        val Password:String = txtPassword.text.toString()

        if (!TextUtils.isEmpty(user) && !TextUtils.isEmpty(Password))
            progressBar.visibility = View.VISIBLE
        auth.signInWithEmailAndPassword(user, Password)
                .addOnCompleteListener(this){
                    tarea ->

                    if (tarea.isSuccessful) {
                        action()
                        progressBar.visibility = View.INVISIBLE
                    } else
                        Toast.makeText(this, "Usuario y contraseña incorrectos", Toast.LENGTH_LONG).show()
                    progressBar.visibility = View.INVISIBLE
                }


    }

    private fun action(){
        startActivity(Intent(this,MainActivity::class.java))
    }

}
